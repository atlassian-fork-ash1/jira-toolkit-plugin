package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import junit.framework.TestCase;
import org.easymock.MockControl;

import java.util.Map;

/**
 * Tests the NullCFType class
 *
 * @since v3.12
 */
public class TestNullCFType extends TestCase {

    /**
     * Tests that the velocity parameters map contains the correct flags given a combination of
     * permissions and workflow state of an issue.
     */
    public void testIsEditableCorrectlySet() {
        final boolean[] options = new boolean[]{false, true};

        // set up mocks
        final MockControl mockJiraAuthenticationContextControl = MockControl.createControl(JiraAuthenticationContext.class);
        final JiraAuthenticationContext mockJiraAuthenticationContext = (JiraAuthenticationContext) mockJiraAuthenticationContextControl.getMock();

        final MockControl mockIssueManagerControl = MockControl.createControl(IssueManager.class);
        final IssueManager mockIssueManager = (IssueManager) mockIssueManagerControl.getMock();

        final MockControl mockPermissionManagerControl = MockControl.createControl(PermissionManager.class);
        final PermissionManager mockPermissionManager = (PermissionManager) mockPermissionManagerControl.getMock();

        final MockControl mockIssueControl = MockControl.createControl(Issue.class);
        final Issue mockIssue = (Issue) mockIssueControl.getMock();

        final NullCFType nullCFType = new NullCFType(mockPermissionManager, mockJiraAuthenticationContext, mockIssueManager);

        mockJiraAuthenticationContext.getUser();
        mockJiraAuthenticationContextControl.setDefaultReturnValue(null);
        mockJiraAuthenticationContextControl.replay();

        // test all combinations of isEditable and hasPermission (FF, FT, TF, TT)
        for (int i = 0; i < options.length; i++) {
            for (int j = 0; j < options.length; j++) {
                boolean hasPermission = options[i];
                boolean isEditable = options[j];

                // set expected values
                mockIssueManagerControl.expectAndReturn(mockIssueManager.isEditable(mockIssue), isEditable);
                mockPermissionManagerControl.expectAndReturn(mockPermissionManager.hasPermission(Permissions.EDIT_ISSUE, mockIssue, (ApplicationUser) null), hasPermission);
                mockPermissionManagerControl.expectAndReturn(mockPermissionManager.hasPermission(Permissions.COMMENT_ISSUE, mockIssue, (ApplicationUser) null), hasPermission);
                // need this because getVelocityParams() super requires it
                mockIssue.getGenericValue();
                mockIssueControl.setDefaultReturnValue(null);

                mockIssueManagerControl.replay();
                mockPermissionManagerControl.replay();
                mockIssueControl.replay();

                final Map<String, Object> params = nullCFType.getVelocityParameters(mockIssue, null, null);
                boolean ableToEditIssue = ((Boolean) params.get("ableToEditIssue")).booleanValue();
                boolean ableToComment = ((Boolean) params.get("ableToComment")).booleanValue();

                assertEquals(ableToEditIssue, isEditable && hasPermission);
                assertEquals(ableToComment, hasPermission);

                // reset mocks for next iteration
                mockIssueManagerControl.reset();
                mockPermissionManagerControl.reset();
                mockIssueControl.reset();
            }
        }
    }
}
