package com.atlassian.jira.toolkit;

/**
 * Holds contstants
 */
public interface RequestAttributeKeys {
    public static final String MULTIKEY_SEARCHING = "MULTIKEYSEARCHING";
    public static final String MULTIKEY_ACTION = "MULTIKEYACTION";
}
