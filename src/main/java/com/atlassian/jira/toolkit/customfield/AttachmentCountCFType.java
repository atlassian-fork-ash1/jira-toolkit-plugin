package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.velocity.NumberTool;

import java.util.List;
import java.util.Map;

public class AttachmentCountCFType extends CalculatedCFType implements SortableCustomField {
    private final AttachmentManager attachmentManager;
    private static final Double NO_ATTACHMENTS = new Double(0);

    public AttachmentCountCFType(AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }

    public String getStringFromSingularObject(Object value) {
        return value != null ? value.toString() : "0";
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException {
        if (string != null) {
            return new Double(string);
        } else {
            return NO_ATTACHMENTS;
        }
    }

    public Object getValueFromIssue(CustomField field, Issue issue) {
        if (issue != null) {
            List attachments = attachmentManager.getAttachments(issue);
            if (attachments != null) {
                return new Double(attachments.size());
            }
        }

        return NO_ATTACHMENTS;
    }

    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
        map.put("numberTool", new NumberTool());
        return map;
    }
}
