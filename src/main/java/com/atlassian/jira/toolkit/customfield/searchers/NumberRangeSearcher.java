package com.atlassian.jira.toolkit.customfield.searchers;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * @since JIRA 4.0
 */
public class NumberRangeSearcher extends com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher {

    public NumberRangeSearcher(final JqlOperandResolver jqlOperandResolver, final DoubleConverter doubleConverter, final CustomFieldInputHelper customFieldInputHelper, final I18nHelper.BeanFactory i18nFactory, final FieldVisibilityManager fieldVisibilityManager) {
        super(jqlOperandResolver, doubleConverter, customFieldInputHelper, i18nFactory, fieldVisibilityManager);
    }
}
